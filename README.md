# coder-desafio-40

Capas, Factory y Singleton

## Estructura del proyecto:

```
.
├── __test__
│   ├── __mock__
│   ├── dal
│   │   └── integration
│   ├── services
│   └── utils
├── imgs
├── logs
├── performance
│   └── report
├── src
│   ├── auth
│   ├── config
│   ├── controllers
│   ├── dal
│   │   ├── memory
│   │   │   ├── Dao
│   │   │   └── repositories
│   │   └── mongoose
│   │       ├── Dao
│   │       ├── Dto
│   │       ├── db
│   │       ├── models
│   │       └── repositories
│   ├── graphql
│   │   ├── resolvers
│   │   └── schema
│   ├── resources
│   ├── routes
│   ├── services
│   ├── utils
│   └── views
│       ├── layouts
│       └── partials
└── target
```

## Inicio del server

inicia el servidor con la persistencia de mongo
`npm start MONGO`

inicia el servidor con la persistencia en memoria
`npm start MEMORY`

### url:

`http://localhost:8080/ingreso`

## Factory

Métdodo para devolver los DAO según el tipo de persistencia que recibe desde consola:

La implementación está en el siguiente archivo:

`src/dal/index.js`

```javascript
const { STORAGE } = require("../config/globals");

/**
 * @description Método factory para seleccionar el tipo de persistencia
 * si opt no existe toma el parámetro que llega desde storage
 * @param {String} opt
 * @returns
 */
const factory = (opt) => {
  if (opt === "MEMORY") return require("./memory");
  else if (opt === "MONGO") return require("./mongoose");
  else if (STORAGE === "MEMORY") return require("./memory");
  else if (STORAGE === "MONGO") return require("./mongoose");
  else return require("./memory");
};

module.exports = factory;
```

el método se llama en el siguiente archivo

`src/services/index.js`

```javascript
let persistence = require("../dal")();
```

Los dos tipos de persistencias `memory` y `mongoose`) están implementadas dentro de la siguiente estructura:

```
src/dal
      ├── memory
      │   ├── Dao
      │   └── repositories
      └── mongoose
          ├── Dao
          ├── Dto
          ├── db
          ├── models
          └── repositories
```

## Validación de factory

Para este punto se escribió un unit test que valida que el singleton funcione correctamente al invocarse el factory con la misma opción elegida

Para una posterior comprobación del singleton agrego una property más en uno de los daos

src/dal/memory/Dao/productDao.js

```javascript
  signature: Math.random(),
```

A continuación lo valido con un test de integración

```javascript
describe("dal validation", () => {
  test("validate singleton implementation for dal", async () => {
    //pido los objetos de persistencia a un factory
    const dao = require("../../../src/dal")("MEMORY");
    //agregué una property en el productDao: 'signature' que recibe un random cuando se instancia por primera vez
    const productDao = dao.productDao;
    //vuelvo a pedir nuevamente los objetos de persistencia a un factory
    const newDao = require("../../../src/dal")("MEMORY");
    //me traigo nuevamente el productDao
    const newProductDao = newDao.productDao;
    //valido que el signature es el mismo nro random generado la primera vez
    expect(productDao.signature).toBe(newProductDao.signature);
  });
});
```

el test pasa correctamente

```
  dal validation
    ✓ validate singleton implementation for dal (40 ms)
```

### test y coverage

los test se corren con el comando

`npm test`

que referencia a la siguiente tarea en el package.json

`"test": "jest --coverage"`

```
 PASS  __test__/services/product.test.js
  service product unit test
    ✓ getAllProducts(): it should return an array of elements when they exist (5 ms)
    ✓ getAllProducts(): the product returned should contain id, item, price and thumbnail (1 ms)
    ✓ getAllProducts() it should return an empty array when there's no result (3 ms)
    ✓ getProduct() it should return null when a given product is not found (1 ms)
    ✓ getProduct() it should return null when a non-mongo id kind is being sent
    ✓ getProduct() it should return an item with a valid id (4 ms)
    ✓ addProduct() it should return the add information when it's added (3 ms)
    ✓ updateProduct() it should return a valid object after updating (6 ms)
    ✓ deleteProduct() it should return a valid response after deleting a valid object (43 ms)
    ✓ should return undefined when model fails (63 ms)
  dal validation
    ✓ validate singleton implementation for dal (40 ms)



-----------------------------|---------|----------|---------|---------|-------------------
File                         | % Stmts | % Branch | % Funcs | % Lines | Uncovered Line #s
-----------------------------|---------|----------|---------|---------|-------------------
All files                    |   61.32 |    49.01 |      50 |   62.42 |
 __test__/__mock__           |     100 |      100 |     100 |     100 |
  item-found-data.js         |     100 |      100 |     100 |     100 |
  item-stored-data.js        |     100 |      100 |     100 |     100 |
  items-found-data.js        |     100 |      100 |     100 |     100 |
  no-items-found.js          |     100 |      100 |     100 |     100 |
 __test__/utils              |    93.1 |     87.5 |     100 |   92.59 |
  fake-dao.js                |    93.1 |     87.5 |     100 |   92.59 | 28,39
 src/config                  |     100 |    58.82 |     100 |     100 |
  globals.js                 |     100 |    58.82 |     100 |     100 | 9-16
 src/dal                     |   41.66 |     12.5 |     100 |      50 |
  index.js                   |   41.66 |     12.5 |     100 |      50 | 11-14
 src/dal/memory              |     100 |      100 |     100 |     100 |
  index.js                   |     100 |      100 |     100 |     100 |
 src/dal/memory/Dao          |   20.83 |        0 |    12.5 |   19.04 |
  index.js                   |     100 |      100 |     100 |     100 |
  productDao.js              |   11.53 |        0 |   11.11 |    8.33 | 3-43
  userDao.js                 |   16.66 |        0 |   14.28 |   14.28 | 3-27
 src/dal/memory/repositories |    30.3 |        0 |      20 |   26.66 |
  baseRepository.js          |   11.53 |        0 |   11.11 |    8.33 | 3-43
  index.js                   |     100 |      100 |     100 |     100 |
  messageRepository.js       |     100 |      100 |     100 |     100 |
 src/services                |     100 |      100 |     100 |     100 |
  product.js                 |     100 |      100 |     100 |     100 |
 src/utils                   |     100 |      100 |     100 |     100 |
  loggers.js                 |     100 |      100 |     100 |     100 |
-----------------------------|---------|----------|---------|---------|-------------------
```
