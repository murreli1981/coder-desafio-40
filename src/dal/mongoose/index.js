const dao = require("./Dao");
const repositories = require("./repositories");

module.exports = {
  ...dao,
  ...repositories,
};
