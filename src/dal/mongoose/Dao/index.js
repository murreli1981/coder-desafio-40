const models = require("../models");
const userDao = require("./userDao");
const productDao = require("./productDao");

module.exports = {
  userDao: userDao(models),
  productDao: productDao(models),
};
