const baseRepository = (model) => ({
  async getAll(element) {
    const elements = await model.find();
    return elements;
  },

  async getById(id) {
    const element = await model.findById(id);
    return element;
  },

  async create(element) {
    const elementCreated = await model.create(element);
    return elementCreated;
  },

  async delete(id) {
    const elementDeleted = await model.findByIdAndDelete(id);
    return elementDeleted;
  },

  async update(id, payload) {
    const elementUpdated = await model.findByIdAndUpdate(id, payload);
    return elementUpdated;
  },
});

module.exports = baseRepository;
