const userDao = ({ userModel }) => ({
  async getByUsername(username) {
    const user = userModel.find((user) => user.username === username);
    return user;
  },

  async getById(id) {
    const user = userModel.find((user) => id.toString() === user.id);
    return user;
  },

  async create(user) {
    const id = userModel.length + 1 + "";
    const userToCreate = {
      ...user,
      id,
    };
    userModel.push(userToCreate);
    return userToCreate;
  },

  async findOrCreate(newUser) {
    const user = this.getByUsername(newUser.username);
    if (user) return user;
    else {
      const userToCreate = this.create(newUser);
      return userToCreate;
    }
  },
});

module.exports = userDao;
