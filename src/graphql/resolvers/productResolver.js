const productResolver = ({ productService }) => ({
  products: async () => {
    try {
      const productsFetched = await productService.getAllProducts();
      return productsFetched;
    } catch (error) {
      throw error;
    }
  },

  product: async (args) => {
    const { id } = args;
    try {
      const productFetched = await productService.getProduct(id);
      return productFetched;
    } catch (error) {
      throw error;
    }
  },

  createProduct: async (args) => {
    try {
      console.log("probando desde graphql");
      console.log(args);
      const { title, price, thumbnail } = args.product;
      const productCreated = await productService.addProduct({
        title,
        price,
        thumbnail,
      });
      return productCreated;
    } catch (error) {
      throw error;
    }
  },
});

module.exports = productResolver;
