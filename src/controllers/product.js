const { productService } = require("../services");

const logger = require("../utils/loggers");
const log = logger.getLogger("default");
const logFile = logger.getLogger("file");

exports.createProduct = async (req, res, next) => {
  log.warn(`Attempting to save Product ${req.body.title}`);
  const product = await productService.addProduct(req.body);
  console.log(product);
  log.info(`product saved with id ${product.id}`);
  req.io.emit("list:products", await productService.getAllProducts());
  res.redirect("/ingreso");
};

exports.listProducts = async (req, res, next) => {
  log.warn("Attempting to list products");
  const products = await productService.getAllProducts();
  log.info(`Products listed: ${products.length}`);
  res.json(products);
};

exports.updateProduct = async (req, res, next) => {
  log.warn(`Attempting to update product ${req.params.id}`);
  const {
    params: { id },
    body,
  } = req;
  const productUpdated = await productService.updateProduct(id, body);
  if (productUpdated === null) {
    log.error(`Product not found with id: ${id}`);
    logFile.error(`Product not found with id: ${id}`);
    res.json({ msg: "product not found" });
  } else {
    log.info(`Product updated with id: ${id}`);
    req.io.emit("list:products", await productService.getAllProducts());
    res.json({ msg: "product updated" });
  }
};

exports.getProduct = async (req, res, next) => {
  log.warn(`Attempting to get product with id ${req.params.id}`);
  console.log(req.params);
  const { id } = req.params;
  const product = await productService.getProduct(id);
  try {
    if (product === null) {
      log.error(`product not found with id: ${id}`);
      logFile.error(`product not found with id: ${id}`);

      res.json({ msg: "product not found" });
    } else {
      log.info(`Product found with id ${product.id}`);
      res.json(product);
    }
  } catch (error) {
    log.error(`not able to check with id ${req.params.id}`);
    logFile.error(`not able to check with id ${req.params.id}`);
  }
};

exports.deleteProduct = async (req, res, next) => {
  log.warn(`Attempting to delete ${req.params.id}`);
  const { id } = req.params;
  const productDeleted = await productService.deleteProduct(id);
  if (productDeleted === null) {
    log.error(`Product not found with id: ${id}`);
    res.json({ msg: "product not found" });
  } else {
    log.info(`Product deleted with id: ${req.params.id}`);
    req.io.emit("list:products", await productService.getAllProducts());
    res.json({ msg: "product deleted" });
  }
};
