const userService = ({ userDao }) => ({
  async getUserByUsername(username) {
    return await userDao.getByUsername(username);
  },

  async getUserById(id) {
    return await userDao.getById(id);
  },

  async createUser(user) {
    return await userDao.create(user);
  },

  async findOrCreateUser(newUser) {
    const user = await userDao.findOrCreate(newUser);
    return user;
  },
});

module.exports = userService;
