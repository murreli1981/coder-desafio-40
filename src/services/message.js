const moment = require("moment");
const { normalizeMessages, print } = require("../utils/normalizer");
const { v4: uuidv4 } = require("uuid");

const messageService = ({ messageRepository }) => ({
  async addMessage(message) {
    message["date"] = `[${moment().format("DD/MM/YYYY hh:mm:ss")}]`;
    message["id"] = Date.now();

    const messageCreated = await messageRepository.create(message);
    return messageCreated;
  },
  async getAllMessages() {
    const messages = await messageRepository.getAll();
    const messageCenter = messageCenterBuilder(messages);
    const normalizedMsgs = normalizeMessages(messageCenter);
    return normalizedMsgs;
  },
});

messageCenterBuilder = (messages) => {
  return { id: 1, content: messages };
};

module.exports = messageService;
