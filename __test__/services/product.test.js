const productService = require("../../src/services/product");
const fakeDao = require("../utils/fake-dao");

describe("service product unit test", () => {
  test("getAllProducts(): it should return an array of elements when they exist", async () => {
    const items = require("../__mock__/items-found-data");

    const service = productService({ productDao: fakeDao(items) });
    const response = await service.getAllProducts();
    expect(Array.isArray(response)).toBeTruthy();
  });

  test("getAllProducts(): the product returned should contain id, item, price and thumbnail", async () => {
    const items = require("../__mock__/items-found-data");
    const service = productService({ productDao: fakeDao(items) });
    const response = await service.getAllProducts();
    const expectedKeys = ["_id", "title", "price", "thumbnail"];
    expectedKeys.map((key) => expect(Object.keys(response[0])).toContain(key));
  });

  test("getAllProducts() it should return an empty array when there's no result", async () => {
    const items = require("../__mock__/no-items-found");
    const service = productService({ productDao: fakeDao(items) });
    const response = await service.getAllProducts();
    expect(Array.isArray(response)).toBeTruthy();
    expect(response.length).toBe(0);
  });

  test("getProduct() it should return null when a given product is not found", async () => {
    const items = require("../__mock__/items-found-data");
    const service = productService({ productDao: fakeDao(items) });
    const response = await service.getProduct("6139825db6d7f6a9d19ba916");
    expect(response).toBe(null);
  });

  test("getProduct() it should return null when a non-mongo id kind is being sent", async () => {
    const items = require("../__mock__/items-found-data");
    const service = productService({ productDao: fakeDao(items) });
    const response = await service.getProduct(
      "invalidIdinvalidIdinvalidIdinvalidIdinvalidIdinvalidId"
    );
    expect(response).toBe(null);
  });

  test("getProduct() it should return an item with a valid id", async () => {
    const item = require("../__mock__/item-found-data");
    const service = productService({ productDao: fakeDao(item) });
    const response = await service.getProduct("validId");
    expect(response).toEqual(item);
  });

  test("addProduct() it should return the add information when it's added", async () => {
    const item = require("../__mock__/item-stored-data");
    const service = productService({ productDao: fakeDao(item) });
    const { title, price, thumbnail } = item;
    const response = await service.addProduct({ title, price, thumbnail });
    expect(response).toEqual(item);
  });

  test("updateProduct() it should return a valid object after updating", async () => {
    const item = require("../__mock__/item-stored-data");
    const service = productService({ productDao: fakeDao(item) });
    const title = "edited";
    const { price, thumbnail } = item;
    const response = await service.updateProduct("validId", {
      title,
      price,
      thumbnail,
    });
    expect(response).toBe(item);
  });

  test("deleteProduct() it should return a valid response after deleting a valid object", async () => {
    const item = require("../__mock__/item-stored-data");
    const service = productService({ productDao: fakeDao(item) });
    const response = await service.deleteProduct("validId");
    expect(response).toBe(item);
  });

  test("should return undefined when model fails", async () => {
    const service = productService({ productDao: fakeDao(null) });
    await expect(service.getAllProducts()).resolves.toBe(undefined);
    await expect(
      service.addProduct({
        item: "test",
        price: "999",
        thumbnail: "http://url.com/test",
      })
    ).resolves.toBe(null);
    await expect(service.getProduct("232323232")).resolves.toBe(null);
    await expect(
      service.updateProduct("validId", { title: "edited" })
    ).resolves.toBe(null);
    await expect(service.deleteProduct("validId")).resolves.toBe(null);
  });
});
